
import tkinter
from math import sin,cos,sqrt, radians, asin
from time import sleep, time
from random import random
from concurrent.futures.process import ProcessPoolExecutor
from concurrent.futures.thread import ThreadPoolExecutor

class EntityManager:

	def __init__(self):
		self.entities = {}
		self.nextId = 0
		self.changed = True

	def getId(self):
		x = self.nextId
		self.nextId += 1
		return x

	def deleteEntity(self, entityId):
		if entityId in self.entities:
			self.entities[entityId].destroy()
			del self.entities[entityId]
			self.changed = True


	def addEntity(self, entity):
		entity.entityId = self.getId()
		self.entities[entity.entityId] = entity
		self.changed = True

	def getEntity(self, entityId): return self.entities[entityId]

	def hasEntity(self, entityId): return entityId in self.entities


	def getEntities(self): 
		if self.changed:
			self.entitiesCache = [x for x in self.entities.values()]
			self.changed = False	
		return self.entitiesCache


class Geometry:

	@staticmethod
	def movement(x, y, speed, direction):
		
		angle = radians(direction)

		opp = speed * sin(angle)
		adj = speed * cos(angle)

		xx = x + adj
		yy = y + opp

		return xx, yy


	@staticmethod
	def unitVector(vector):
		length = sqrt(sum([v*v for v in vector]))

		return [v/length for v in vector]

	@staticmethod
	def directionVector(start, finish):
		return [f-s for (s,f) in zip(start,finish)]

	@staticmethod
	def intersection(start, finish, x=None, y=None):
		d = Geometry.directionVector(start, finish)

		sx = start[0]
		sy = start[1]

		dx = d[0]
		dy = d[1]

		if x != None:
			m = (x - sx) / dx
		elif y != None:
			m = (y - sy) / dy
		else:
			raise Exception("need an x or y line")

		ix = sx + m * dx
		iy = sy + m * dy

		return ix, iy


	@staticmethod
	def distance(v1, v2):
		return sqrt( sum([(x1-x2)**2 for (x1,x2) in zip(v1, v2)]) )


	@staticmethod
	def mapCoordinates(sWidth, sHeight, fWidth, fHeight, sX, sY):
		xScale = fWidth / sWidth
		yScale = fHeight / sHeight
		return (sX * xScale, sY * yScale)

		




class World:

	CacheClear = 20

	def __init__(self, width, height, entities):
		self.entityRatios = entities
		self.width = width
		self.height = height
		self.xRange = range(self.width+1)
		self.yRange = range(self.height+1)
		self.em = EntityManager()
		self.locationBoard = [[[] for y in range(height+1)] for x in range(width+1)]   
		self.initEntities(entities)
		self.clear = 0
		self.turn = 0

	def randomLocation(self):
		return int(random() * self.width), int(random() * self.height)

	def clearBoard(self):
		b = self.locationBoard
		for x in range(self.width+1):
			for y in range(self.height+1):
				b[x][y] = []

	def boardLocations(self, entity):
		midSize = int((entity.size-1) / 2)
		locations = [(x,y) for x in range(int(entity.x)-midSize, int(entity.x)+midSize+1) for y in range(int(entity.y)-midSize, int(entity.y)+midSize+1) if x in self.xRange and y in self.yRange]
		return locations

	def addToBoard(self, entity):
		for (x,y) in self.boardLocations(entity):
			self.locationBoard[x][y].append(entity.entityId)
		
	def removeFromBoard(self, entity):
		for (x,y) in self.boardLocations(entity):
			list = self.locationBoard[x][y]
			if entity.entityId in list:
				list.remove(entity.entityId)

	def initEntities(self, entities):

		for type, frequency in entities:
			for index in range(frequency):
				entity = type(self,*self.randomLocation())
				self.addEntity(entity)

	def update(self):
		self.turn += 1

		if self.clear <= 0:
			self.clear = self.CacheClear
			self.clearBoard()

			for entity in self.em.getEntities():
				self.addToBoard(entity)
		else:
			self.clear -= 1


		for entity in self.em.getEntities():
			entity.update()    

		for entity in self.em.getEntities():
			self.moveEntity(entity)

	def getEntities(self): return self.em.getEntities()

	def addEntity(self, entity):
		self.em.addEntity(entity)
		self.addToBoard(entity)

	def deleteEntity(self, entityId): 
		entity = self.em.getEntity(entityId)
		self.em.deleteEntity(entityId)
		self.removeFromBoard(entity)
		

	def moveEntity(self,entity):
		width = self.width
		height = self.height
		sx, sy, speed, direction = entity.x, entity.y, entity.speed, entity.direction
		xx, yy = Geometry.movement(sx, sy, speed, direction)

		if xx < 0 or xx > width or yy < 0 or yy > height:

			if xx < 0:
				ix = 0
			elif xx > width:
				ix = width
			else:
				ix = None

			if yy < 0:
				iy = 0
			elif yy > height:
				iy = height
			else:
				iy = None

			

			try:
				xx, yy = Geometry.intersection((sx, sy), (xx, yy), x=ix, y=iy)
				direction = int(random() * 360)
			except ZeroDivisionError:
				print("intersection calculation attempted between two lines that don't intersect")
			
		
		entity.direction = direction
		entity.speed = speed
		entity.x = xx
		entity.y = yy

	def adjacent(self, entity):
		entities = []
		for (x,y) in self.boardLocations(entity):
			entities.extend(self.locationBoard[x][y])
		
		if entity.entityId in entities:
			entities.remove(entity.entityId)

		return [self.em.getEntity(entityId) for entityId in entities if self.em.hasEntity(entityId)]
		
		

	def __repr__(self):
		return "\n".join([repr(e) for e in self.em.getEntities()])

				




class Entity:

	def name(self): return self.__class__.__name__
	size = 5
	speed = 1
	color = "white"
	shape = "oval"



	def __init__(self, world, x, y):

		self.world = world
		self.x = x
		self.y = y

		self.direction = int(360 * random())

		self.setup()

	def update(self): pass

	def setup(self): pass

	def draw(self, windowWidth, windowHeight):
		f = lambda x,y: Geometry.mapCoordinates(self.world.width, self.world.height, windowWidth, windowHeight, x, y)	
		topLeftX = self.x - self.size / 2
		topLeftY = self.y - self.size / 2
		bottomRightX = self.x + self.size / 2
		bottomRightY = self.y + self.size / 2

		tlx, tly = f(topLeftX, topLeftY)
		brx, bry = f(bottomRightX, bottomRightY)

		tly = windowHeight - tly
		bry = windowHeight - bry

		drawer =  "create_{}".format(self.shape)

		return (drawer,[(tlx, tly, brx, bry)], {"fill":self.color})


	def destroy(self): pass

	def __repr__(self):
		return "{}[x={}, y={}, direction={}, speed={}]".format(self.name, self.x, self.y, self.direction, self.speed)




class Cell(Entity): 

	color = "green"
	age = 0
	speed = 2
	size = 5

	food = 0
	CellCost = 5

	def setup(self):
		self.maxAge = int(random() * 400)

	def update(self):

		food = [f for f in self.world.adjacent(self) if f.name() == "Food"]

		if len(food) > 0:
			self.world.deleteEntity(food[0].entityId)
			self.food += 1
			self.size += 1

			if self.food >= self.CellCost:
				self.food -= self.CellCost
				self.size -= self.CellCost
				self.world.addEntity(Cell(self.world,self.x, self.y))

		self.age += 1
		if self.age > self.maxAge:
			for x in range(self.size):
				self.world.addEntity(Food(self.world,self.x, self.y))
			self.world.deleteEntity(self.entityId)





class Food(Entity): 

	color = "yellow"
	size = 2
	speed = 0.1
	stationary = False






class Render:

	ups = 30
	running = False
	quitted = False

	Title = "Siddall's Cell Simulator (Turn: {}, Cells: {}, Food: {})"

	def __init__(self, world, width, height):
		self.world = world
		self.root = tkinter.Tk()
		self.root.resizable(0,0)
		
		self.updateTitle()
		self.interface = tkinter.Frame(self.root,width=width, height=100)
		self.interface.pack(expand=tkinter.YES,fill=tkinter.BOTH)

		self.exitButton = tkinter.Button(self.interface,text="quit",command=self.quit)
		self.exitButton.pack(side=tkinter.LEFT)

		self.startButton = tkinter.Button(self.interface, text="start",command=self.run)
		self.startButton.pack(side=tkinter.LEFT)

		self.stopButton = tkinter.Button(self.interface, text="stop", command=self.stop)
		self.stopButton.pack(side=tkinter.LEFT)

		self.resetButton = tkinter.Button(self.interface, text="reset", command=self.reset)
		self.resetButton.pack(side=tkinter.LEFT)

		self.widthLabel = tkinter.Label(self.interface, text="width: ")
		self.widthLabel.pack(side=tkinter.LEFT)
		self.widthEntry = tkinter.Entry(self.interface,width=5)
		self.widthEntry.pack(side=tkinter.LEFT)
		self.widthEntry.insert(0, self.world.width)

		self.heightLabel = tkinter.Label(self.interface, text="height: ")
		self.heightLabel.pack(side=tkinter.LEFT)
		self.heightEntry = tkinter.Entry(self.interface,width=5)
		self.heightEntry.pack(side=tkinter.LEFT)
		self.heightEntry.insert(0, self.world.height)

		self.cellsLabel = tkinter.Label(self.interface, text="cells: ")
		self.cellsLabel.pack(side=tkinter.LEFT)
		self.cellsEntry = tkinter.Entry(self.interface,width=5)
		self.cellsEntry.pack(side=tkinter.LEFT)
		self.cellsEntry.insert(0, [freq for (type, freq) in world.entityRatios if type == Cell][0])
		
		self.foodLabel = tkinter.Label(self.interface, text="food: ")
		self.foodLabel.pack(side=tkinter.LEFT)
		self.foodEntry = tkinter.Entry(self.interface,width=5)
		self.foodEntry.pack(side=tkinter.LEFT)
		self.foodEntry.insert(0, [freq for (type, freq) in world.entityRatios if type == Food][0])


		self.frame = tkinter.Frame(self.root,width=width, height=height)
		self.frame.pack(expand=tkinter.YES,fill=tkinter.BOTH)
		self.canvas = tkinter.Canvas(self.frame,width=width, height=height)
		self.canvas.pack(expand=tkinter.YES,fill=tkinter.BOTH)

		self.running = False
		self.quitted = False

		self.pool = ThreadPoolExecutor(max_workers=4)

	def updateTitle(self):
		entities = self.world.getEntities()
		cells = 0
		food = 0
		for entity in entities:
			if entity.name() == "Food":
				food += 1
			elif entity.name() == "Cell":
				cells += 1
		self.root.wm_title(self.Title.format(self.world.turn, cells, food))

	def schedule(self):
		f = lambda: self.root.after(int(1000/self.ups), self.update) 
		self.root.after_idle(f)

	def start(self):
		self.schedule()
		self.root.mainloop()

	def run(self):
		if not self.running:
			self.running = True
			self.schedule()

	def reset(self):
		ratios = [(Cell, int(self.cellsEntry.get())), (Food, int(self.foodEntry.get()))]
		self.world = World(int(self.widthEntry.get()), int(self.heightEntry.get()), ratios)

	def stop(self):
		self.running = False

	def quit(self):
		self.running = False
		self.quitted = True

	def update(self):
			
		if self.running:
			#s = time()
			self.world.update()
			#duration = (time() - s) * 1000
			#mups = 1 / duration
			#print("update took {}, max ups is {}".format(duration, mups))
				
		if self.quitted:
			self.root.quit()
		else:
			#s = time()
			self.draw()
			#duration = (time() - s) * 1000
			#print("draw took {}".format(duration))
			self.updateTitle()
			self.schedule()
		
		

	def draw(self):
		self.canvas.delete("all")

		de = lambda e: e.draw(self.canvas.winfo_width(), self.canvas.winfo_height())
		
		for callback, args1, args2 in [de(e) for e in self.world.getEntities()]:
			f = getattr(self.canvas, callback)
			f(*args1,**args2)





size = (50, 50)
cells = max(10,int(size[0]*size[1] * 0.005))
foods = max(100, int(size[0]*size[1] * 0.1 ))
entities = [(Cell, cells), (Food, foods)]
world = World(500, 500, entities)

render = Render(world, 600, 600)
render.start()
